﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

[Serializable]
public class Content
{
    public Text chatText;
    public InputField replyInputField;
    public Button replyButton;
    public GameObject chatContainer;
    public GameObject playerModel;
    public List<AudioClip> questionAudios;
}
public class ScreenManagerScript : MonoBehaviour {

    public Content content;

    private Animator playerModelAnimator;
    private AudioSource m_AudioSource;
    private string chatTextContent = "";
    private List<string> questions = new List<string>();
    private List<List<string>> possibleAnswers = new List<List<string>>();
    private int currentQuestion = 0;
    private bool canReply = false;
    private Button lastButton;

    private List<Button> replyButtons = new List<Button>();

	// Use this for initialization
	void Start () {

        AddCustomerQuestions();
        AddUserPossibleReplies();
        playerModelAnimator = content.playerModel.GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
        StartCoroutine(AskQuestion(questions[currentQuestion]));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void AddCustomerQuestions()
    {
        questions.Add("I need to update the address on my insurance policy.");
        questions.Add("My name is Lucas Sulka and my policy number is KR5678-345. Yes, I'm moving this weekend.");
        questions.Add("I'm sorry, I have meeting soon. Is there a way we can speed this up?");
    }

    private void AddUserPossibleReplies()
    {
        List<string> tempListOne = new List<string>();
        List<string> tempListTwo = new List<string>();
        List<string> tempListThree = new List<string>();

        tempListOne.Add("Sure");
        tempListOne.Add("I can help you with that!");
        tempListOne.Add("Have you moved recently?");
        tempListOne.Add("What is your new address?");
        tempListOne.Add("Do you have your policy number?");
        tempListOne.Add("My name is Leisha Field");
        tempListOne.Add("May I have your name?");

        tempListTwo.Add("I'm looking up your policy now.");
        tempListTwo.Add("Are you interested in learning more about our life insurance offerings?");
        tempListTwo.Add("What is your new address?");
        tempListTwo.Add("Are you excited about the move?");
        tempListTwo.Add("Do you want to change your coverage?");

        tempListThree.Add("No problem!");
        tempListThree.Add("What is your new address?");
        tempListThree.Add("I'm working as fast as I can.");
        tempListThree.Add("Would you like me to call you back later?");

        possibleAnswers.Add(tempListOne);
        possibleAnswers.Add(tempListTwo);
        possibleAnswers.Add(tempListThree);

        //tempList.Clear();
    }

    private IEnumerator AskQuestion(string question)
    {
        if (currentQuestion >= 2)
        {
                playerModelAnimator.SetBool("isSad", true);
        }

        chatTextContent += "\n\nLucas: ";
        chatTextContent += question;
        yield return new WaitForSeconds(1f);
        content.chatText.text = chatTextContent;

        Canvas.ForceUpdateCanvases();
        content.chatContainer.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
        Canvas.ForceUpdateCanvases();

        m_AudioSource.Stop();
        m_AudioSource.PlayOneShot(content.questionAudios[currentQuestion]);

        playerModelAnimator.SetBool("isTalking", true);
        StartCoroutine(stopTalking(content.questionAudios[currentQuestion].length));

        GenerateReplies();
        currentQuestion++;
        canReply = true;

        //foreach (char c in question)
        //{
        //    chatTextContent += c;
        //    if(content.chatText)
        //    content.chatText.text = chatTextContent;
        //    yield return new WaitForSeconds(0.1f);
        //}   
    }

    private void GenerateReplies()
    {
        foreach (string s in possibleAnswers[currentQuestion])
        {
            Button b = Instantiate(content.replyButton, Vector3.zero, content.replyButton.transform.rotation) as Button;
            var bTransform = b.GetComponent<RectTransform>();
            bTransform.SetParent(GameObject.FindWithTag("ChatContainer").transform);
            b.GetComponentInChildren<Text>().text = s;

            bTransform.anchoredPosition = new Vector2(70, -80);

            if (replyButtons.Count > 0)
            {
                bTransform.anchoredPosition = new Vector2(replyButtons[replyButtons.Count - 1].GetComponentInChildren<Text>().preferredWidth +
                    replyButtons[replyButtons.Count - 1].GetComponent<RectTransform>().anchoredPosition.x + 30f, -80);
            }

            bTransform.localScale = new Vector3(1, 1, 1);
            b.onClick.AddListener(delegate { OnClickReplyTemplate(b); });

            replyButtons.Add(b);
        }
    }

    public void OnClickReplyTemplate(Button b)
    {
        if (!content.replyInputField.text.Contains(b.GetComponentInChildren<Text>().text))
            content.replyInputField.text += " " + b.GetComponentInChildren<Text>().text;

        else
            content.replyInputField.text =content.replyInputField.text.Replace(" " +b.GetComponentInChildren<Text>().text, "");
    }

    public void OnSubmit()
    {
        if (!canReply || content.replyInputField.text == "")
            return;


        chatTextContent += "\n\nYou: " + content.replyInputField.text;
        content.chatText.text = chatTextContent;
        canReply = false;

        Canvas.ForceUpdateCanvases();
        content.chatContainer.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
        Canvas.ForceUpdateCanvases();

        foreach (Button b in replyButtons)
        {
            Destroy(b.gameObject, 0);
        }
        replyButtons.Clear();

        content.replyInputField.text = "";

        if ((currentQuestion > questions.Count - 1))
            return;

        StartCoroutine(AskQuestion(questions[currentQuestion]));       
    }

    public void OnClickRestartButton()
    {
        StartCoroutine(LoadLevelByName(SceneManager.GetActiveScene().name));
    }

    private IEnumerator LoadLevelByName(string levelName)
    {
       var loadLevelTask = SceneManager.LoadSceneAsync(levelName);

        yield return loadLevelTask;
    }

    private IEnumerator stopTalking(float time)
    {
        yield return new WaitForSeconds(time);
        playerModelAnimator.SetBool("isTalking", false);
    }
}
