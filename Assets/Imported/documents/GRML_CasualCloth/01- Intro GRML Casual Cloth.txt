GRML Casual Cloth.

This pack is a part of GR Modular System which allowed to construct character from separate parts -- add new body and/or cloth parts to "base" character.

IMPORTANT: "GRML Casual Cloth" pack does not include character model and/or body parts and to use it You should have one of "GRML Base ..." packs which can be found in my section of store. 

All models in GRML series have identical skeleton: 
- Main bones - 52;
- Twist bones - 8;
- Facial bones - 32;
- Weapon Dummy - 4 (optional);
- Ground contact Dummy - 12 (optional).

The pack includes 11 cloth models and 41 prefabs with different fashions/colors:
- Upper 5 (20 prefabs);
- Lower 3 (12 prefabs);
- Shoes 2 (6 prefabs);
- Accessory 1 (3 prefabs).
All models are middle poly, model with maximal poly-counter has 3K vertices and 6K triangular polygons.
Models in pack oriented on use in games and videos which required high resolution and close look. They used Standard PBS materials and textures (Color, Metallic, Smoothness, Normal Bump, Occlusion) with upto 2K resolution. Normal Bump maps have micro-surface details for more natural cloth look.

Also in pack:
- GRConstruct editor script for adding part to avatar;
- GRML_BonePuppet model (used in example scenes);
- Example scene and Documentation.

If You will have any questions please ask them in my forum post -
http://forum.unity3d.com/threads/vit3d-character-s-for-unity.343517
and I'll be glad to answer them.