GRML Casual Cloth Version History
----------------------------------

V1.1
-----
- Textures have been tuned to fit Unity 5.3 Smoothness curve.
- The pack now has unified folders structure and naming rules for better compatibility with all products from GR series.

v1.0
-----
First release.