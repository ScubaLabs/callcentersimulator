Texture Tuning Masks.
----------------------

Files in this folder oriented on Adobe Photoshop users and can be useful for customization textures included in Gr packs.

1.0 What is Texture Tuning Masks.

As You know RGB image file can includes up-to 4 independent channels -- Red, Green, Blue and optional Alpha. Each mask file in this folder is not a single mask, but set of masks because each channel in file defines different regions in texture file -- fabric, leather, metal or some specific elements. Using these masks in Photoshop You can easy convert flatten texture image into file with layers and tune each layer independently.
IMPORTANT: By default empty Alpha channel filled by white (not by black as other RGB channels) and masks stored in Alpha channel have been inverted for future compatibility.

2.0 How to use.

Here step-by-step instruction of Texture Tuning Masks usage.

1. Open texture file and corresponding mask file in Photoshop.
2. Duplicate Background in texture file and select Layer > Layer Mask >Reveal all from menu.
3. Select Red channel in Channels palette of mask file. Press Ctrl+A (select all) and Ctrl+C (Copy).
4. Select new layer with mask in texture file; switch to Channels palette, select mask channel and press Ctrl+V (Paste). 
5. If mask file has other not empty channels repeat steps 2-4 for each of them (Green, Blue, Alpha). When You will create next copy of background always move it as top layer and do not forgot to invert mask copied from Alpha channel (Ctrl+I).

As result You will have this layer structure:
- background copy with inverted mask from Alpha;
- background copy with mask from Blue;
- background copy with mask from Green;
- background copy with mask from Red;
- Background.
Now You can tune each layer independently for texture customization. 

Trick: If You prefer to work with "traditional" layers without masks You can do this:
- Create empty layer under layer with mask.
- Select both layer with mask and new empty layer and press Ctrl+E (Merge Layers).