GR Hair Shader Read Me.
-----------------------

GR Hair Shader is custom modification of PBS Standard (Specular) shader for not solid semi-transparent models visualization.
It used the same set of textures as original, but Height, Emission and Secondary maps do not used for hair visualization in this pack.

As original, GR Hair shader has full set of "Rendering Modes", but only two of them ("Transparent" and "Fade") should be used for hair.

Recommended Mode is "Transparent" (used in this pack) -- it produce more physically correct result, but, as with original Standard (Specular) shader, You should keep in mind that Specular and Smoothness textures should be "cut to black" using the same Transparency map to avoid "glass effect".

"Fade" mode has no such requirement for Specular and Smoothness textures construction, but it does not use full set of Unity 5 visualization features. 
