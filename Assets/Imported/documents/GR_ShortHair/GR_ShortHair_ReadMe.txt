GR Short Hair Read Me.
----------------------

1.0 Introduction.

This pack is a part of GR Modular System and hair models in it have been created for head models from "GRML Base ..." and "GRFL Base ..." packs.

IMPORTANT: "GR Short Hair" pack does not include character model and/or body parts and to use it You should have one of Base packs which can be found in my section of store.
On pictures hair models shown with heads from Base packs.

The pack includes three hair models (for fitting to different head shapes) rigged to male and female skeletons and 6 materials (hair colors). 
Model resolution -- 4304 vertices (5086 triangular polygons).
Materials uses custom modification of PBS Standard (Specular) shader for not solid semi-transparent models visualization.
Textures resolution = 2K (Color+Transparency, Specular+Smoothness, Normal, AO).

The pack also includes:
- Photoshop Specular construction file (with layers) for hair color customization;
- Additional layer file for head ambient occlusion texture modification (when it will be used with hair);
- GR Hair shader with documentation.

2.0 Heads fitting.

You should use GRConstruct editor script from "GRML Base ..." pack to add hear model to character. There three models for fitting to different head shapes - "A", "B" and "F".
Currently hair with "A" mark should be used with "Gregory" head, "B" with Timothy head and "F" with female heads. In the future (if more head shapes will be added to GR product line) this information will updated.

3.0 Tricks and Tips.

"GRML Base ..." packs have 2 head materials -- "Bald" and "Shaved". Keep in mind that some hair colors will look more natural if You will use "Bald" material for head and another (usually more dark hair) - "Shaved" material.

If character has hair You can modify head AO texture for more natural look:
- Open original head AO file in Photoshop;
- Open GR_ShortHair_Head_AO from this pack, select corresponding AO_From_Hair layer, copy it and paste on top of original head AO texture;
- Merge layers, save new file with some different name and use it creating character head material with hair.

The pack included 6 hair color materials, but You can create custom hair colors by combining and/or modifying existed color textures in Photoshop.
To have more natural look after color modification You should also modify Specular texture. Easy way to do this is to use GR_ShortHair_Spec_Construct.psd file included in the pack:
- Pick most dark color from new hair color map and use it for filling  "Color_Dark" layer in construct file;
- Pick most light color from new hair color map and use it for filling  "Color_Light" layer in construct file (do not change layer mask);
- Save copy of construct file as flatten image and add Alpha channel (Smoothness) from one of existed files.
This is simplest "raw" method and for fine result slightly tune "Dark" and "Light" colors.


More products from GRML series can be found in my section of store -
"https://www.assetstore.unity3d.com/en/#!/publisher/15211"
GR Series products discussions can be found in "Vit3D Character(s) for Unity" forum post -
"http://forum.unity3d.com/threads/vit3d-character-s-for-unity.343517/"