GRConstcuct ReadMe.
------------------------------

The GRConstruct editor script is the "key component" of the "GR Modular System". This script allows the construction of characters from separate parts � that is to say that one can add new body and/or cloth parts to a "base" character.
After installation the script can be found in menu "GR Tools > GRConstruct".

IMPORTANT: The GRConstruct script has been tested only with models from the "GR Modular System" and I'm not able to guarantee that there will be no issues if used with other models. 

- 1. GRConstruct GUI -

GRConstruct GUI is very simple and easy to use:

"Base Avatar" parameter (game object)-
Game object to which new skinned mesh part will be added.

"Part to Add" parameter (game object)-
Game object which skinned mesh will be used as new part for "Base Avatar".

"Auto Prefabs" option -
By default this option turned OFF. If You turn it ON, after adding new part, the script will automatically create a prefab of result. 
The Prefab will have the name <avatar game object name>_<part game object name>.prefab and stored in the root of Your project Assets folder.

"Add Part to Avatar" button -
This button initiate construction process.
If You forgot to set one of parameters You will see warning - 
"Define all(!) parameters :".
If all parameters defined and correct, new part will be added and message changed to -
"Done. You can define new Part to Add :"
The script automatically clears "Part to Add" parameter to allow the construction to by dragging next part to this field. 

Notes: After adding to base avatar new skinned mesh object will have the same name as the name of game object selected as "Part to Add", not the name of source skinned mesh.

- 2. Requirements and Recommendations. -

The main requirement for script to work normally is that both "Base Avatar" and "Par to Add" game objects must have exactly the same skeleton (same hierarchy and bone names). 

The second requirement in the current version of script is that "Part to Add" game object should have only one skinned mesh -- In each construction step current version allow to add only one skinned mesh object at a time.
To fit this requirement always use game objects with several mesh parts as "Base Avatar" (this can be - body parts with head or results of previous constructions) and game objects with single mesh part as "Part to Add".

After finishing construction the resultant combined game object will be in the scene, but it's not recommended to use it directly, you should create a prefab of result for future use. 
The easy way to do this is to use "Auto Prefab" script feature -- At last step of construction turn ON "Auto Prefab" option and press "Add Part to Avatar" button.
Prefab of Your construction will be created automatically and You, if You wish, just rename it and move to corresponding folder in the project. 

And last recommendations: 
- To avoid any issues, prefabs, but not source models, should be used in construction process and the name of prefab must be different from the name of source model. 

- 3. Step-by-step example. -

Here the recommended way for GRConstcuct script usage:

1. Create empty scene (or use scene from Examples\ConstructDemo folder).

2. Drag and drop all prefabs of models You planned to use in final avatar in scene Hierarchy window (already done in example scene).

3. Start GRConstruct script (menu GR Tools > GRConstruct).

4. Choose game object which will be used as "Base Avatar" and drag it to corresponding script field. As mentioned above "Base Avatar" should be game object with several mesh parts (usually model which included head).

5. Choose game object which will be used as "Part to Add" and drag it to corresponding script field. Be sure that You drag game object but not a skinned mesh object and it has only one skinned mesh object in hierarchy.

6. If this is the last step in avatar creation, turn "Auto Prefab" ON.

7. If all parameters correct You will see new skinned mesh part in "Base Avatar" hierarchy and message (on the top of script window) will change to "Done. You can define new Part to Add :"

8. If You have not finished construction , go to Step 5 and choose next part.

9. If You added all parts, close script.

If You exactly followed this step-by-step instruction, You will find prefab of construction result in the root of Assets folder. If You wish You can rename this prefab and move it into corresponding project folder. It's not required but recommended do not change first 5 symbols in prefab name.
Note: There is no need to keep construction scene.

Hope You found this instruction useful. 
If You will have any questions please ask them in my forum post -
http://forum.unity3d.com/threads/vit3d-character-s-for-unity.343517
and I'll be glad to answer them.

