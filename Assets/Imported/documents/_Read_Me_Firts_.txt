_Read_Me_First.
------------------------------

Please read these notes before installation to avoid misunderstanding.

1. All models in packs have been rigged with "Blend Weights" limit = 4 bones and most of textures have 2K resolution. Be sure that Your project settings fit to these requirements.

2. Content from all "GR Modular System" series packs installed in the GR folder and if one pack already installed in Your project You can got warning messages during next pack installation. This happened because some packs have absolutely identical components and Unity replace already installed components.
In fact there is no error at all, just save project after installation and load it again.